package com.qagroup.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.qagroup.car.Car;

public class CarTest {

	private Car car1 = new Car("Mazeratti", "Red");;
	
	@Test
	public void test1() {
		car1.changeColor("Blue");
		Assert.assertNotNull(car1);
	}
	
	@Test(dependsOnMethods = "test1")
	public void test0( ) {
		Assert.assertEquals(car1.getColor(), "Blue");
	}

}

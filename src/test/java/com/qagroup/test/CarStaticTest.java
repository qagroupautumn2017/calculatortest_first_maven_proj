package com.qagroup.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.qagroup.car.Car;

public class CarStaticTest {

	@Test(priority = 1)
	public void test1() {
		Car car1 = new Car("", "");
		Assert.assertNotNull(car1);
	}

	@Test(priority = 2)
	public void testCarCounter() {
		int initCounter = Car.numberOfCreatedCars();
		new Car("", "");
		new Car("", "");
		new Car("", "");
		int finCounter = Car.numberOfCreatedCars();
		int expectedResult = initCounter + 3;
		Assert.assertEquals(finCounter, expectedResult);
	}

}

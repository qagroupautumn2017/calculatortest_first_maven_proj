package com.qagroup.test;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.qagroup.arrays.Calculator;

public class CalculatorTest {

	@Test
	public void testAllPositiveValues() {
		int[] array = { 1, 2, 3 };
		int expectedResult = 6;

		Calculator calculator = new Calculator();
		int actualResult = calculator.calculateProduct(array);

		Assert.assertEquals(actualResult, expectedResult);
	}

	@Test
	public void testWithNegativeValue() {
		int[] array = { -1, 2, 3 };
		int expectedResult = -6;

		Calculator calculator = new Calculator();
		int actualResult = calculator.calculateProduct(array);

		Assert.assertEquals(actualResult, expectedResult);
	}

	@Test
	public void testWithZeroValue() {
		int[] array = { 0, 2, 3 };
		int expectedResult = 0;

		Calculator calculator = new Calculator();
		int actualResult = calculator.calculateProduct(array);

		Assert.assertEquals(actualResult, expectedResult);
	}

	@Test(expectedExceptions = { Exception.class }, description = "Expect exception to be thrown")
	public void testWithEmptyArray() {
		int[] array = {};

		Calculator calculator = new Calculator();
		calculator.calculateProduct(array);
	}
}

package com.qagroup.arrays;

public class Calculator {
	public int calculateProduct(int[] array) {
		if (array.length == 0)
			throw new RuntimeException("Provided array should not be empty");
		
		int product = 1;

		for (int element : array) {
			product = product * element;
		}

		return product;
	}
}

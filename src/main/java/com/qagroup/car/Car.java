package com.qagroup.car;

public class Car {

	private static final int MAXIMUM_SPEED = 120;

	private static int counter = 0;

	private String model;

	private String color;

	private int currentSpeed;

	public Car(String whichModel, String whichColor) {
		counter++;
		this.model = whichModel;
		this.color = whichColor;
	}

	public static int numberOfCreatedCars() {
		return counter;
	}

	public String getModel() {
		return this.model;
	}

	public String getColor() {
		return this.color;
	}

	public void changeColor(String newColor) {
		this.color = newColor;
	}

	public int getSpeed() {
		return this.currentSpeed;
	}

	public void setSpeed(int speed) {
		if (speed <= MAXIMUM_SPEED) {
			this.currentSpeed = speed;
		} else {
			this.currentSpeed = MAXIMUM_SPEED;
		}
	}

	public void accelerateBy(int increase) {
		this.currentSpeed = currentSpeed + increase;
		if (this.currentSpeed > MAXIMUM_SPEED) {
			this.currentSpeed = MAXIMUM_SPEED;
		}
	}

	public void info() {
		defaultMethod();
		System.out.println("Model: " + model + "; Color: " + color + "; Current speed: " + currentSpeed);
	}

	void defaultMethod() {
		System.out.println("Default access method call");
		privateMethod();
	}

	private void privateMethod() {
		System.out.println("Private method call");
	}
}